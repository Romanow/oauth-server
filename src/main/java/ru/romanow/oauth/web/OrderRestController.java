package ru.romanow.oauth.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.romanow.oauth.domain.Order;
import ru.romanow.oauth.service.OrderService;
import ru.romanow.oauth.web.model.OrderRequest;
import ru.romanow.oauth.web.model.OrderResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by romanow on 03.11.16
 */
@RestController
@RequestMapping("/api/order")
public class OrderRestController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public OrderResponse getOrder(@PathVariable String id) {
        return new OrderResponse(orderService.getById(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<OrderResponse> findAll() {
        return orderService.findAll()
                           .stream()
                           .map(OrderResponse::new)
                           .collect(Collectors.toList());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public void createOrder(@RequestBody OrderRequest orderRequest, HttpServletResponse response) {
        Order order = orderService.save(orderRequest);
        response.addHeader(HttpHeaders.LOCATION, "/order/" + order.getId());
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteOrder(@PathVariable String id) {
        orderService.delete(id);
    }
}
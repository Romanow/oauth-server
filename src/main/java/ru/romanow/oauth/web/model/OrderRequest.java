package ru.romanow.oauth.web.model;

import com.google.common.base.MoreObjects;

/**
 * Created by romanow on 03.11.16
 */
public class OrderRequest {
    private Integer sum;
    private Integer discount;

    public Integer getSum() {
        return sum;
    }

    public Integer getDiscount() {
        return discount;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .omitNullValues()
                          .add("sum", sum)
                          .add("discount", discount)
                          .toString();
    }
}

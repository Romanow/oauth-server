package ru.romanow.oauth.web.model;

import com.google.common.base.MoreObjects;
import ru.romanow.oauth.domain.Order;

/**
 * Created by romanow on 03.11.16
 */
public class OrderResponse {
    private Integer sum;
    private Integer discount;

    public OrderResponse() {}

    public OrderResponse(Order order) {
        this.sum = order.getSum();
        this.discount = order.getDiscount();
    }

    public Integer getSum() {
        return sum;
    }

    public Integer getDiscount() {
        return discount;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .omitNullValues()
                          .add("sum", sum)
                          .add("discount", discount)
                          .toString();
    }
}

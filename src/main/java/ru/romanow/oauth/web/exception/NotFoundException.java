package ru.romanow.oauth.web.exception;

/**
 * Created by romanow on 03.11.16
 */
public class NotFoundException
        extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }
}

package ru.romanow.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Authorization Code
 * http://127.0.0.1:8080/oauth/authorize?response_type=code&client_id=test-client&redirect_uri=https://ya.ru&scope=read
 *
 * Token
 * http://127.0.0.1:8080/oauth/token?code=code&scope=read&grant_type=authorization_code&redirect_uri=https://ya.ru
 * Header
 * Authorization: Basic base64(test-client + 123)
 */
@SpringBootApplication
public class Application
        extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
package ru.romanow.oauth.domain;

import com.google.common.base.MoreObjects;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by romanow on 03.11.16
 */
@Document(collection = "order")
public class Order {

    @Id
    private String id;
    private Integer sum;
    private Integer discount;

    public String getId() {
        return id;
    }

    public Integer getSum() {
        return sum;
    }

    public Order setSum(Integer sum) {
        this.sum = sum;
        return this;
    }

    public Integer getDiscount() {
        return discount;
    }

    public Order setDiscount(Integer discount) {
        this.discount = discount;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .omitNullValues()
                          .add("sum", sum)
                          .add("discount", discount)
                          .toString();
    }
}

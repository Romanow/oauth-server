package ru.romanow.oauth;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Collections;

/**
 * Created by romanow on 07.11.16
 */
@Order(-20)
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration
        extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin().loginPage("/login")
                        .loginProcessingUrl("/check-auth")
                        .usernameParameter("login")
                        .passwordParameter("password")
                        .permitAll()
            .and()
            .authorizeRequests()
                        .antMatchers("/login",
                                     "/static/**",
                                     "/check-auth",
                                     "/oauth/authorize")
                        .permitAll()
            .and()
            .authorizeRequests()
                        .anyRequest()
                        .authenticated()
            .and()
            .csrf().disable();
    }

    @Override
    @Bean(name = "userDetailsService")
    public InMemoryUserDetailsManager userDetailsService() {
        return new InMemoryUserDetailsManager(
                Collections.singletonList(new User("1", "1", Collections.emptyList())));
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService());
    }
}

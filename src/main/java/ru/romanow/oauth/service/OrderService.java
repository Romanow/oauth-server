package ru.romanow.oauth.service;

import ru.romanow.oauth.domain.Order;
import ru.romanow.oauth.web.model.OrderRequest;

import java.util.List;

/**
 * Created by romanow on 03.11.16
 */
public interface OrderService {
    Order getById(String id);

    List<Order> findAll();

    Order save(OrderRequest orderRequest);

    void delete(String id);
}

package ru.romanow.oauth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.romanow.oauth.domain.Order;
import ru.romanow.oauth.repository.OrderRepository;
import ru.romanow.oauth.web.exception.NotFoundException;
import ru.romanow.oauth.web.model.OrderRequest;

import java.util.List;

/**
 * Created by romanow on 03.11.16
 */
@Service
public class OrderServiceImpl
        implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Override
    @Transactional(readOnly = true)
    public Order getById(String id) {
        Order order = orderRepository.findOne(id);
        if (order == null) {
            throw new NotFoundException("Order '{" + id + "}' not found");
        }
        return order;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    @Transactional
    public Order save(OrderRequest orderRequest) {
        Order order = new Order()
                .setSum(orderRequest.getSum())
                .setDiscount(orderRequest.getDiscount());

        return orderRepository.save(order);
    }

    @Override
    @Transactional
    public void delete(String id) {
        orderRepository.delete(id);
    }
}

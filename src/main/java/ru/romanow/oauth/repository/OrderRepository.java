package ru.romanow.oauth.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.romanow.oauth.domain.Order;

/**
 * Created by romanow on 03.11.16
 */
public interface OrderRepository
        extends MongoRepository<Order, String> {}

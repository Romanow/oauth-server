package ru.romanow.oauth;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by romanow on 03.11.16
 */
@Configuration
@EnableMongoRepositories
public class DataConfiguration {}
### ЛР #2. API и OAuth2.0 авторизация

#### Сборка и запуск
Для запуска требуется MongoDB. В комплекте идет embedded-mongo,
дистрибутив автоматически скачивается и запускается на 27018 порту.
Если есть предустановленный mongo-server, то в build.gradle надо закомментировать строку
`compile "de.flapdoodle.embed:de.flapdoodle.embed.mongo"` и в `application.properties` прописать параметры подключения.
Для сборки и запуска выполнить `./gradlew clean bootRun`. Сервер запустится на 8080 порту.

#### Сценарии работы
Сервис предоставляет методы API:

1. Открытые методы
http://127.0.0.1:8080/ping

1. Закрытые CRUD методы на сущность Order
http://127.0.0.1:8080/api/order
При запросе к методам требуется передать хедер `Authorization: Bearer <token>`,
если хедер будет отсутствовать или токен будет некорректным,
то вернется 401 ошибка с телом ответа (для `Accept: application/json`)

```
#!json

{
  "error": "unauthorized",
  "error_description": "Full authentication is required to access this resource"
}
```
 
#### Получение токена
Получение токена делится на два этапа: получение кода и обмен кода на токен.
Вручную создан клиент clientId: `test-client`, secret: `123`, redirectUri: `https://ya.ru`, и пользователь login: `1`, password: `1`. 

Выполнить запрос `http://127.0.0.1:8080/oauth/authorize?response_type=code&client_id=test-client&redirect_uri=https://ya.ru&scope=read`

Если пользователь не авторизован, то будет редирект на страницу авторизацию. В случае успешной авторизации
будет выполнен редирект на redirectUri `https://ya.ru?code=<code>`.

На втором этапе выполнить запрос `http://127.0.0.1:8080/oauth/token?code=code&scope=read&grant_type=authorization_code&redirect_uri=https://ya.ru`
и в хедере передать basic-авторизацию `Authorization: Basic Base64(clientId + secret)`.
В ответ будет отдан json:

```
#!json

{
    "access_token": "93c8bb6b-ea71-4dd6-b122-da512522dbc0",
    "token_type": "bearer",
    "refresh_token": "2ad2f098-c6ca-4620-bfc5-860b84e53506",
    "expires_in": 43199,
    "scope": "read"
}
```


#### Уточнение
В примере использовался фреймворк Spring Security OAuth.
В лабораторной требуется реализовать этот функционал без сторонних библиотек.